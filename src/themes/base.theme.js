import colorList, { colors } from "./color";

export default {
  colors: colors({
    primary: colorList.chestnutRose,
    secondary: colorList.white,
    text: colorList.black
  })
};
