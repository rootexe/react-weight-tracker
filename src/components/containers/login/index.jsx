import React, { memo } from "react";
import { FormattedMessage } from "react-intl";
import { Button } from "@blueprintjs/core";
import firebase, { googleProvider } from "./../../../helpers/firebase";

const LoginWithGoogle = () => firebase.auth().signInWithRedirect(googleProvider);

export default memo(() => (
  <FormattedMessage id="login">{local => <Button onClick={LoginWithGoogle.bind(this)}>{local}</Button>}</FormattedMessage>
));
